#ifndef MYLIB_H
#define MYLIB_H

#ifndef __cplusplus
    #include <stdbool.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

bool is_even(int n);

#ifdef __cplusplus
}
#endif

#endif  // MYLIB_H
