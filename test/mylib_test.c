#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "mylib.h"

#define TEST(cond) { \
        if (!cond) { \
            fprintf(stderr, "%s %d: Failed on %s\n", __FILE__, __LINE__, #cond); \
            exit(1); \
        } \
    }

int main(void)
{
    TEST(is_even(3) == false);
    TEST(is_even(4) == true);
    
    return 0;
}
